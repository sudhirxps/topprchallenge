package co.sudhirmishra.topprchallenge;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import static android.R.id.shareText;
import static android.webkit.WebViewDatabase.getInstance;

public class EventDetail extends AppCompatActivity {
    String shareText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent detail = getIntent();
        Event event = (Event) detail.getParcelableExtra("event");

        TextView desc = (TextView)findViewById(R.id.desc);
        TextView header = (TextView)findViewById(R.id.header);
        TextView exp = (TextView)findViewById(R.id.experience);
        TextView category = (TextView)findViewById(R.id.category);
        NetworkImageView image = (NetworkImageView)findViewById(R.id.networkImageView);

        setTitle(event.name);
        desc.setText(event.description);
        header.setText(event.name);
        category.setText("Category " + event.category);
        exp.setText("Experience: " + event.experience);
        ImageLoader loader = MySingleton.getInstance(this).getImageLoader();
        image.setImageUrl(event.image, loader);
        image.setDefaultImageResId(R.drawable.ic_default_image);
        image.setErrorImageResId(R.drawable.ic_default_image);

        shareText = ""+ event.name + " "+event.description;

        // Share feature from https://developer.android.com/training/sharing/send.html
        Button shareButton = (Button)findViewById(R.id.shareButton);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });



    }

}
