package co.sudhirmishra.topprchallenge;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sudhirxps on 24/9/16.
 */

public class Event implements Parcelable {

    public String name;
    public String id;
    public String image;
    public String category;
    public String description;
    public String experience;


    protected Event(Parcel in) {
        name = in.readString();
        id = in.readString();
        image = in.readString();
        category = in.readString();
        description = in.readString();
        experience = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(image);
        dest.writeString(category);
        dest.writeString(description);
        dest.writeString(experience);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}