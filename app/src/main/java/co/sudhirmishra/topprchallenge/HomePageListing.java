package co.sudhirmishra.topprchallenge;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static co.sudhirmishra.topprchallenge.HackerEarthService.KEY_AVAILABLE;
import static co.sudhirmishra.topprchallenge.HackerEarthService.KEY_MAX;
import static co.sudhirmishra.topprchallenge.HackerEarthService.KEY_WEBSITE;
import static co.sudhirmishra.topprchallenge.R.id.rvEvents;

public class HomePageListing extends AppCompatActivity {
    // Volley request queue
    private RequestQueue mRequestQueue;
    // API response, list of events
    ArrayList<Event> events;
    // View for listing events
    RecyclerView rvEvents;
    // API Quota
    TextView quota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_listing);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRequestQueue = MySingleton.getInstance(this.getApplicationContext()).
                getRequestQueue();


        rvEvents = (RecyclerView) findViewById(R.id.rvEvents);
        quota = (TextView) findViewById(R.id.quote);

        // Make the api call
        fetchJsonResponse();

        Button catButton = (Button) findViewById(R.id.categorySort);
        catButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortAdapter(new CategoryComparator());
            }
        });

        Button name = (Button) findViewById(R.id.favSort);
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortAdapter(new NameComparator());
            }
        });
    }
    // Utility for sorting
    private void sortAdapter(Comparator c){
        EventsAdapter adapter = (EventsAdapter)rvEvents.getAdapter();
        List<Event> events = adapter.getEvents();
        Collections.sort(events,c);
        adapter.setEvents(events);
        adapter.notifyDataSetChanged();
    }
    // Sort by category
    static class CategoryComparator implements Comparator<Event>{
        public int compare(Event lhs, Event rhs){
            return lhs.category.compareTo(rhs.category);
        }
    }
    // Fav-star not implemented. Use name to toggle the view
    static class NameComparator implements Comparator<Event>{
        public int compare(Event lhs, Event rhs){
            return lhs.name.compareTo(rhs.name);
        }
    }
    private void fetchJsonResponse() {

        JsonObjectRequest req = new JsonObjectRequest(HackerEarthService.BASE_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // Response is a JSONObject with keys, websites, quote_max, quote_available
                            String result = response.getString(KEY_WEBSITE);

                            Gson gson = new Gson();
                            Event[] eventsArray = gson.fromJson(result,Event[].class);
                            List<Event> events = Arrays.asList(eventsArray);

                            // Create adapter passing in the sample user data
                            EventsAdapter adapter = new EventsAdapter(HomePageListing.this, events);
                            // Attach the adapter to the recyclerview to populate items
                            rvEvents.setAdapter(adapter);
                            // Set layout manager to position the items
                            rvEvents.setLayoutManager(new LinearLayoutManager(HomePageListing.this));

                            int quote_max = Integer.parseInt(response.get(KEY_MAX).toString());
                            int quote_available = Integer.parseInt(response.get(KEY_AVAILABLE).toString());

                            quota.setText("API Quota "+(quote_max - quote_available)*100/quote_max + "%");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });

		/* Add your Requests to the RequestQueue to execute */
        mRequestQueue.add(req);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_page_listing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            //Redirect to Web
            String url = "http://sudhirmishra.github.com";
            Intent webIntent = new Intent(Intent.ACTION_VIEW);
            webIntent.setData(Uri.parse(url));
            startActivity(webIntent);

        }

        return super.onOptionsItemSelected(item);
    }
}
