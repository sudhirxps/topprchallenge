package co.sudhirmishra.topprchallenge;

/**
 * Created by sudhirxps on 24/9/16.
 */

public interface HackerEarthService {

    public static final String BASE_URL = "https://hackerearth.0x10.info/api/toppr_events?type=json&query=list_events";
    public static final String KEY_WEBSITE = "websites";
    public static final String KEY_MAX= "quote_max";
    public static final String KEY_AVAILABLE = "quote_available";
}
