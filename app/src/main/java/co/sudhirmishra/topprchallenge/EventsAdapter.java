package co.sudhirmishra.topprchallenge;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.w3c.dom.Text;

import java.util.List;

import static android.media.CamcorderProfile.get;

/**
 * Created by sudhirxps on 24/9/16.
 */

public class EventsAdapter  extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    // Store a member variable for the contacts
    static List<Event> events;
    // Store the context for easy access
    private Context mContext;

    // Pass in the contact array into the constructor
    public EventsAdapter(Context context, List<Event> contacts) {
        events = contacts;
        mContext = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }


    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameTextView;
        public TextView categoryTextView;
        public NetworkImageView mNetworkImageView;
        Context context;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(Context context,View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.name);
            categoryTextView = (TextView) itemView.findViewById(R.id.category);
            mNetworkImageView = (NetworkImageView) itemView.findViewById(R.id.networkImageView);
            this.context = context;
            itemView.findViewById(R.id.detail_button).setOnClickListener(this);
        }
        // Handles the row being being clicked
        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            // Check if an item was deleted, but the user clicked it before the UI removed it
            if (position != RecyclerView.NO_POSITION) {

                // Get the clicked object
                Event event = events.get(position);

                // Send the clicked event object to EventDetail
                Intent detailIntent = new Intent(context ,EventDetail.class);
                detailIntent.putExtra("event",event);
                context.startActivity(detailIntent);

            }
        }
    }
    @Override
    public EventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.row_layout, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(context,contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(EventsAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        Event event = events.get(position);

        // Set item views based on your views and data model
        TextView textView = viewHolder.nameTextView;
        textView.setText(event.name);
        viewHolder.categoryTextView.setText(event.category);


        ImageLoader mImageLoader;

        // Get the NetworkImageView that will display the image.

        // Get the ImageLoader through your singleton class.
        mImageLoader = MySingleton.getInstance(getContext()).getImageLoader();

        // Set the URL of the image that should be loaded into this view, and
        // specify the ImageLoader that will be used to make the request.
        viewHolder.mNetworkImageView.setImageUrl(event.image, mImageLoader);
        viewHolder.mNetworkImageView.setDefaultImageResId(R.drawable.ic_default_image);
        viewHolder.mNetworkImageView.setErrorImageResId(R.drawable.ic_default_image);

    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return events.size();
    }

    public List getEvents() {
        return events;
    }
    public void setEvents(List e){
        events = e;
    }
}
